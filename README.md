# Escape Room

> escape-room

A website for the 2019 Technology Student Association "Website Design" challenge
with the theme "An escape from the Ordinary."

!["Escape Room" logo wordmark](/images/wordmark.png)